DROP DATABASE if EXISTS coti;
CREATE DATABASE IF NOT EXISTS coti;

USE coti;

CREATE TABLE product(
	product_id INT NOT NULL PRIMARY KEY,
	product_name VARCHAR(100) NOT NULL,
	product_price FLOAT(5,2) unsigned NOT NULL
);

INSERT INTO product (product_id, product_name, product_price) values
	(1, "Dulce de Leche La Serenisima", 25.50),
	(2, "Café Instantaneo Arlistán", 65.20),
	(3, "Salsa Filleto El Serrano", 32.78),
	(4, "Aciete El Girasol", 14.00),
	(5, "Queso Untable Finlandia", 28.43);

CREATE TABLE stock(
	stock_id INT NOT NULL PRIMARY KEY,
	stock_quantity INT NOT NULL,
	stock_min INT NOT NULL,
	stock_product_id INT NOT NULL,
	FOREIGN KEY (stock_product_id) REFERENCES product(product_id)
);

INSERT INTO stock values(1,100,10,1);
INSERT INTO stock values(2,250,25,2);
INSERT INTO stock values(3,50,5,3);
INSERT INTO stock values(4,500,50,4);
INSERT INTO stock values(5,300,30,5);

/*
SELECT product_id,product_name,product_price,stock_quantity,stock_min FROM product P INNER JOIN stock S WHERE P.product_id=S.stock_id;
*/

CREATE TABLE payment_method(
	payment_method_id INT NOT NULL PRIMARY KEY,
	payment_method_name varchar(50) NOT NULL
);

INSERT INTO payment_method (payment_method_id, payment_method_name) values
	(1, "COTI card"),
	(2, "CREADIT card");

CREATE TABLE persona(
	cui INT(15) NOT NULL PRIMARY KEY,
	name varchar(50) NOT NULL,
	type INT NOT NULL
);

INSERT INTO persona (cui,name,type) values
	(16655698, "Martin Lennard", 1),
	(5251287, "Leandro Small", 1),
	(5424987, "Romina Garcia", 1),
	(12876334, "Magesh Prakasam", 2),
	(90443867, "James Kuduva", 2),
	(90443827, "Christian Dasque", 2);

CREATE TABLE transaction_type(
	transaction_type_id INT NOT NULL PRIMARY KEY,
	transaction_type_name VARCHAR(10) NOT NULL
);

INSERT INTO transaction_type (transaction_type_id, transaction_type_name) values
	(1, "sell"),
	(2, "buy");

CREATE TABLE transaction(
	transaction_id INT NOT NULL PRIMARY KEY,
	transaction_cui INT(15) NOT NULL ,
	transaction_payment_method INT NOT NULL,
	transaction_type INT NOT NULL,
	FOREIGN KEY (transaction_cui) REFERENCES persona(cui) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (transaction_payment_method) REFERENCES payment_method(payment_method_id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (transaction_type) REFERENCES transaction_type(transaction_type_id) ON DELETE CASCADE ON UPDATE CASCADE
);

/* INSERTS FOR TRANSACTION TABLE */

CREATE TABLE product_list(
	list_id INT NOT NULL PRIMARY KEY,
	transaction_id INT NOT NULL,
	product_id INT NOT NULL,
	product_quantity INT NOT NULL,
	subtotal FLOAT(4,2) NOT NULL,
	FOREIGN KEY (transaction_id) REFERENCES transaction(transaction_id),
	FOREIGN KEY (product_id) REFERENCES product(product_id)
);


/* INSERTS FOR TRANSACTION TABLE */

